#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define BUFFER_SIZE 256

int main()
{
    int fd1[2]; // pipe 1
    int fd2[2]; // pipe 2
    pid_t pid;

    // create pipe 1
    if (pipe(fd1) == -1) {
        perror("pipe1");
        return 1;
    }

    // create pipe 2
    if (pipe(fd2) == -1) {
        perror("pipe2");
        return 1;
    }

    // fork the process
    pid = fork();
    if (pid == -1) {
        perror("fork");
        return 1;
    }

    // child process
    if (pid == 0) {
        close(fd1[1]);
        close(fd2[0]);

        char buffer[BUFFER_SIZE];
        int n = read(fd1[0], buffer, BUFFER_SIZE);
        if (n == -1) {
            perror("read1");
            return 1;
        }

        buffer[n] = '\0';

        printf("Child (before conversion): %s\n", buffer);

        // convert to uppercase
        for (int i = 0; i < n; i++) {
            buffer[i] = toupper(buffer[i]);
        }

        int m = write(fd2[1], buffer, strlen(buffer));
        if (m == -1) {
            perror("write2");
            return 1;
        }
        close(fd1[0]);
        close(fd2[1]);
    }
    // parent process
    else {
        close(fd1[0]);
        close(fd2[1]);

        char buffer[] = "Hello";
        int n = write(fd1[1], buffer, strlen(buffer));
        if (n == -1) {
            perror("write1");
            return 1;
        }

        char result[BUFFER_SIZE];
        int m = read(fd2[0], result, BUFFER_SIZE);
        if (m == -1) {
            perror("read2");
            return 1;
        }
        result[m] = '\0';
        printf("Parent: %s\n", result);
        wait(NULL);
        close(fd1[1]);
        close(fd2[0]);
    }

    return 0;
}